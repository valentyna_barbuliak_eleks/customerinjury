﻿using CustomerInjuiry.DataServices.Helpers;
using Entities;
using Microsoft.EntityFrameworkCore;

namespace CustomerInjuiry.DataServices
{
    public class CustomerInjuiryContext : DbContext
    {
        public CustomerInjuiryContext(DbContextOptions options) : base(options)
        {

        }

        public DbSet<TransactionEntity> Transactions { get; set; }

        public DbSet<CustomerEntity> Customers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<TransactionEntity>().Property(t => t.Amount).HasPrecision(15, 2);

            builder.Entity<CustomerEntity>().HasIndex(c => c.ContactEmail).IsUnique();
        }
    }
}
