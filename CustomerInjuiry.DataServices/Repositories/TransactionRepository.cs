﻿using CustomerInjuiry.DataServices.Declaration;
using Entities;

namespace CustomerInjuiry.DataServices.Repositories
{
    public class TransactionRepository : BaseRepository<TransactionEntity>, ITransactionRepository
    {
        public TransactionRepository(CustomerInjuiryContext context) : base(context)
        {
        }
    }
}
