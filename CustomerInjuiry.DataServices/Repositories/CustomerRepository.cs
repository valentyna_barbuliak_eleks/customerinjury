﻿using CustomerInjuiry.DataServices.Declaration;
using Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CustomerInjuiry.DataServices.Repositories
{
    public class CustomerRepository : BaseRepository<CustomerEntity>, ICustomerRepository
    {
        public CustomerRepository(CustomerInjuiryContext context) : base(context)
        {
        }
        public async Task<CustomerEntity> GetByCondition(int? id, string email)
        {
            return await _context.Customers.Include(c => c.Transactions).Where(c => c.ContactEmail == email || c.Id == id).FirstOrDefaultAsync();
        }
    }
}
