﻿using CustomerInjuiry.DataServices.Declaration;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace CustomerInjuiry.DataServices.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
            where TEntity : class
    {
        protected readonly CustomerInjuiryContext _context;

        protected BaseRepository(CustomerInjuiryContext context)
        {
            _context = context;
        }

        public void Add(TEntity entity)
        {
            _context.Add(entity);
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Delete(TEntity entity)
        {
            _context.Remove(entity);
        }

        public async Task<TEntity[]> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToArrayAsync();
        }

        public void MarkUpdate(TEntity entity)
        {
            _context.Update(entity);
        }
    }
}
