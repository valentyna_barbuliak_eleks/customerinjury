﻿using CustomerInjuiry.Domain;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CustomerInjuiry.DomainServices.Declaration
{
    public interface ICustomerService
    {
        Task<Customer[]> GetCustomers();

        Task<Customer> GetByCondition(int? id, string email);

        Task<Customer> AddCustomer(Customer customer);

        Task<Customer> UpdateCustomer(Customer customer);
    }
}
