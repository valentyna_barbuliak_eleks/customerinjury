﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInjuiry.DomainServices.Declaration
{
    public interface IObjectMapper
    {
        TResult Map<TInput, TResult>(TInput input);

        object Map(Type inputType, Type outputType, object input);
    }
}
