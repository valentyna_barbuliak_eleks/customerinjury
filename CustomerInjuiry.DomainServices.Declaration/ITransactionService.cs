﻿using CustomerInjuiry.Domain;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInjuiry.DomainServices.Declaration
{
    public interface ITransactionService
    {
        Task<Transaction[]> GetTransactions();

        Task<Transaction> AddTransaction(Transaction transaction);

        Task<Transaction> UpdateTransaction(Transaction transaction);
    }
}
