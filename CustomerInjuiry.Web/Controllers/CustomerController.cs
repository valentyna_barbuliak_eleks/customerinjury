﻿using CustomerInjuiry.Domain;
using CustomerInjuiry.DomainServices.Declaration;
using CustomerInjuiry.Web.ViewModels;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net.Mail;
using System.Threading.Tasks;

namespace CustomerInjuiry.Web.Controllers
{
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerService _customerService;

        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        [HttpGet]
        public async Task<Customer[]> GetCustomers()
        {
            return await _customerService.GetCustomers();
        }

        [HttpPost]
        public async Task<ActionResult<Customer>> AddCustomer([FromBody]Customer customer)
        {
            if (String.IsNullOrEmpty(customer.ContactEmail))
            {
                return BadRequest("Email is required.");
            }

            return await _customerService.AddCustomer(customer);
        }

        [HttpPut]
        public async Task<ActionResult<Customer>> UpdateCustomer([FromBody]Customer customer)
        {
            if (String.IsNullOrEmpty(customer.ContactEmail))
            {
                return BadRequest("Email is required.");
            }

            return await _customerService.UpdateCustomer(customer);
        }

        [HttpGet]
        [Route("CustomerInjuiry")]
        public async Task<ActionResult<Customer>> GetCustomerByInjuiry([FromQuery]CustomerInjuiryModel customerInjuiry)
        {
            if (customerInjuiry == null)
            {
                return BadRequest("No inquiry criteria");
            }

            if (!customerInjuiry.Id.HasValue && String.IsNullOrEmpty(customerInjuiry.Email))
            {
                return BadRequest("No inquiry criteria");
            }

            if (customerInjuiry.Id.HasValue && customerInjuiry.Id <= 0)
            {
                return BadRequest("Invalid Customer ID");
            }

            if (!String.IsNullOrEmpty(customerInjuiry.Email))
            {
                try
                {
                    var address = new MailAddress(customerInjuiry.Email).Address;
                }
                catch (Exception)
                {
                    return BadRequest("Invalid Email");
                }
            }

            try
            {
                Customer customer = await _customerService.GetByCondition(customerInjuiry.Id, customerInjuiry.Email);

                if (customer == null)
                {
                    return NotFound();
                }
                return Ok(customer);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

    }
}
