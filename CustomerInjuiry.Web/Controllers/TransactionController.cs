﻿using CustomerInjuiry.Domain;
using CustomerInjuiry.DomainServices.Declaration;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace CustomerInjuiry.Web.Controllers
{
    [Route("api/[controller]")]
    public class TransactionController : ControllerBase
    {
        private readonly ITransactionService _transactionService;

        public TransactionController(ITransactionService transactionService)
        {
            _transactionService = transactionService;
        }

        [HttpGet]
        public async Task<Transaction[]> GetTransactions()
        {
            return await _transactionService.GetTransactions();
        }

        [HttpPost]
        public async Task<Transaction> AddTransaction([FromBody]Transaction transaction)
        {
            return await _transactionService.AddTransaction(transaction);
        }

        [HttpPut]
        public async Task<Transaction> UpdateTransaction([FromBody]Transaction transaction)
        {
            return await _transactionService.UpdateTransaction(transaction);
        }
    }
}
