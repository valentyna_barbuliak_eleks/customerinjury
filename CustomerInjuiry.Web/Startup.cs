﻿using CustomerInjuiry.DataServices;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using CustomerInjuiry.DataServices.Declaration;
using CustomerInjuiry.DataServices.Repositories;
using CustomerInjuiry.DomainServices;
using CustomerInjuiry.DomainServices.Declaration;
using CustomerInjuiry.DomainServices.Services;
using Swashbuckle.AspNetCore.Swagger;

namespace CustomerInjuiry.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<CustomerInjuiryContext>((provider, builder) =>
            {
                string connectionString = Configuration.GetValue<string>("ConnectionString");
                builder.UseSqlServer(connectionString);
            });

            services.AddSingleton<IObjectMapper>(CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile()));

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ITransactionRepository, TransactionRepository>();

            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<ITransactionService, TransactionService>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Inquiry API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Customer Inquiry API V1");
                c.RoutePrefix = "api/swagger";
            });

            app.UseMvc();
        }
    }
}
