﻿using AutoMapper;
using CustomerInjuiry.Domain;
using CustomerInjuiry.Entities.Helpers;
using Entities;
using System;

namespace CustomerInjuiry.DomainServices
{
    public class CustomerInjuiryProfile : Profile
    {
        public CustomerInjuiryProfile()
        {
            CreateMap<TransactionEntity, Transaction>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.ToString()));

            CreateMap<Transaction, TransactionEntity>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => Enum.Parse<TransactionStatus>(src.Status, true)));

            CreateMap<TransactionEntity, Transaction>()
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => src.CurrencyCode.ToString()));

            CreateMap<Transaction, TransactionEntity>()
                .ForMember(dest => dest.CurrencyCode, opt => opt.MapFrom(src => Enum.Parse<CurrencyCode>(src.CurrencyCode, true)));
        }
    }
}
