﻿using System;
using AutoMapper;
using IObjectMapper = CustomerInjuiry.DomainServices.Declaration.IObjectMapper;

namespace CustomerInjuiry.DomainServices
{
    public class ObjectMapper : IObjectMapper
    {
        private readonly IMapper _mapper;

        internal ObjectMapper(IMapper mapper)
        {
            _mapper = mapper;
        }

        public TResult Map<TInput, TResult>(TInput input) => _mapper.Map<TInput, TResult>(input);
        public object Map(Type inputType, Type outputType, object input) => _mapper.Map(input, inputType, outputType);
    }
}
