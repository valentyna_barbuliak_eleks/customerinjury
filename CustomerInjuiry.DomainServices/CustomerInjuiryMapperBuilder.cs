﻿using AutoMapper;

namespace CustomerInjuiry.DomainServices
{
    public static class CustomerInjuiryMapperBuilder
    {
        public static ObjectMapper Build(Profile profile)
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMissingTypeMaps = true;
                cfg.AddProfile(profile);
            });

            return new ObjectMapper(config.CreateMapper());
        }
    }
}
