﻿using CustomerInjuiry.DataServices.Declaration;
using CustomerInjuiry.Domain;
using CustomerInjuiry.DomainServices.Declaration;
using Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CustomerInjuiry.DomainServices.Services
{
    public class TransactionService : ITransactionService
    {
        private readonly ITransactionRepository _transactionRepository;
        private readonly IObjectMapper _mapper;

        public TransactionService(ITransactionRepository transactionRepository, IObjectMapper mapper)
        {
            _transactionRepository = transactionRepository;
            _mapper = mapper;
        }

        public async Task<Transaction[]> GetTransactions()
        {
            return _mapper.Map<TransactionEntity[], Transaction[]>(await _transactionRepository.GetAllAsync());
        }

        public async Task<Transaction> AddTransaction(Transaction transaction)
        {
            var entity = _mapper.Map<Transaction, TransactionEntity>(transaction);
            _transactionRepository.Add(entity);

            await _transactionRepository.CommitAsync();

            return _mapper.Map<TransactionEntity, Transaction>(entity);
        }

        public async Task<Transaction> UpdateTransaction(Transaction transaction)
        {
            var entity = _mapper.Map<Transaction, TransactionEntity>(transaction);
            _transactionRepository.MarkUpdate(entity);

            await _transactionRepository.CommitAsync();

            return _mapper.Map<TransactionEntity, Transaction>(entity);
        }
    }
}
