﻿using CustomerInjuiry.DataServices.Declaration;
using CustomerInjuiry.Domain;
using CustomerInjuiry.DomainServices.Declaration;
using Entities;
using System.Threading.Tasks;

namespace CustomerInjuiry.DomainServices.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IObjectMapper _mapper;

        public CustomerService(ICustomerRepository customerRepository, IObjectMapper mapper)
        {
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        public async Task<Customer[]> GetCustomers()
        {
            return _mapper.Map<CustomerEntity[], Customer[]>(await _customerRepository.GetAllAsync());
        }

        public async Task<Customer> GetByCondition(int? id, string email)
        {
            return _mapper.Map<CustomerEntity, Customer>(await _customerRepository.GetByCondition(id, email));
        }

        public async Task<Customer> AddCustomer(Customer customer)
        {
            var entity = _mapper.Map<Customer, CustomerEntity>(customer);
            _customerRepository.Add(entity);

            await _customerRepository.CommitAsync();

            return _mapper.Map<CustomerEntity, Customer>(entity);
        }

        public async Task<Customer> UpdateCustomer(Customer customer)
        {
            var entity = _mapper.Map<Customer, CustomerEntity>(customer);
            _customerRepository.MarkUpdate(entity);

            await _customerRepository.CommitAsync();

            return _mapper.Map<CustomerEntity, Customer>(entity);
        }
    }
}
