﻿using CustomerInjuiry.Entities.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInjuiry.Domain
{
    public class Transaction
    {
        public int Id { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Amount { get; set; }

        public string CurrencyCode { get; set; }

        public string Status { get; set; }
    }
}
