﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomerInjuiry.Domain
{
    public class Customer
    {
        public int Id { get; set; }

        public string CustomerName { get; set; }

        public string ContactEmail { get; set; }

        public string MobileNumber { get; set; }

        public Transaction[] Transactions { get; set; }
    }
}
