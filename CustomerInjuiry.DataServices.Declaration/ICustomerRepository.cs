﻿using Entities;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CustomerInjuiry.DataServices.Declaration
{
    public interface ICustomerRepository : IRepository<CustomerEntity>
    {
        Task<CustomerEntity> GetByCondition(int? id, string email);
    }
}
