﻿using System.Threading.Tasks;

namespace CustomerInjuiry.DataServices.Declaration
{
    public interface IRepository<TEntity> where TEntity : class
    {
        Task<TEntity[]> GetAllAsync();

        void Add(TEntity entity);

        void Delete(TEntity entity);

        void MarkUpdate(TEntity entity);

        Task CommitAsync();
    }
}
