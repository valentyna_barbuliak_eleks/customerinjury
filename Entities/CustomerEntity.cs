﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Entities
{
    public class CustomerEntity
    {
        public int Id { get; set; }

        [MaxLength(30)]
        public string CustomerName { get; set; }

        [EmailAddress]
        [MaxLength(25)]
        [Required]
        public string ContactEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        [MaxLength(10)]
        public string MobileNumber { get; set; }

        public virtual ICollection<TransactionEntity> Transactions { get; set; }

    }
}
