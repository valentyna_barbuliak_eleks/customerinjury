﻿using CustomerInjuiry.Entities.Helpers;
using System;

namespace Entities
{
    public class TransactionEntity
    {
        public int Id { get; set; }

        public DateTime TransactionDate { get; set; }

        public decimal Amount { get; set; }

        public CurrencyCode CurrencyCode { get; set; }

        public TransactionStatus Status { get; set; }

    }
}
