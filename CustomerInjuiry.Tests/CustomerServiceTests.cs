using CustomerInjuiry.DataServices.Declaration;
using CustomerInjuiry.DomainServices;
using CustomerInjuiry.DomainServices.Declaration;
using CustomerInjuiry.DomainServices.Services;
using CustomerInjuiry.Web.Controllers;
using CustomerInjuiry.Web.ViewModels;
using Entities;
using FakeItEasy;
using Microsoft.AspNetCore.Mvc;
using System;
using Xunit;

namespace CustomerInjuiry.Tests
{
    public class CustomerServiceTests
    {
        [Fact]
        public void GetByConditionWithIdAndEmail()
        {
            //Arrange
            var mockEntity = new CustomerEntity { Id = 1, ContactEmail = "test" };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());

            A.CallTo(() => repository.GetByCondition(mockEntity.Id, mockEntity.ContactEmail)).Returns(mockEntity);

            var service = new CustomerService(repository, objectMapper);

            //Act
            var result = service.GetByCondition(1, "test").Result;

            //Assert
            Assert.Equal(mockEntity.Id, result.Id);
            Assert.Equal(mockEntity.ContactEmail, result.ContactEmail);
        }

        [Fact]
        public async void GetBadRequestForNoCriteria()
        {
            //Arrange
            var mockEntity = new CustomerInjuiryModel { Id = null, Email = null };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());
            var service = new CustomerService(repository, objectMapper);
            var controller = new CustomerController(service);

            //Act
            var result = await controller.GetCustomerByInjuiry(mockEntity);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.Equal("No inquiry criteria", (result.Result as BadRequestObjectResult).Value);
        }

        [Fact]
        public async void GetBadRequestForInvalidCustomerId()
        {
            //Arrange
            var mockEntity = new CustomerInjuiryModel { Id = -2 };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());
            var service = new CustomerService(repository, objectMapper);
            var controller = new CustomerController(service);

            //Act
            var result = await controller.GetCustomerByInjuiry(mockEntity);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.Equal("Invalid Customer ID", (result.Result as BadRequestObjectResult).Value);
        }

        [Fact]
        public async void GetBadRequestForInvalidEmail()
        {
            //Arrange
            var mockEntity = new CustomerInjuiryModel { Email = "string" };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());
            var service = new CustomerService(repository, objectMapper);
            var controller = new CustomerController(service);

            //Act
            var result = await controller.GetCustomerByInjuiry(mockEntity);

            //Assert
            Assert.IsType<BadRequestObjectResult>(result.Result);
            Assert.Equal("Invalid Email", (result.Result as BadRequestObjectResult).Value);
        }

        [Fact]
        public void GetByConditionWithOnlyId()
        {
            //Arrange
            var mockEntity = new CustomerEntity { Id = 1 };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());

            A.CallTo(() => repository.GetByCondition(mockEntity.Id, null)).Returns(mockEntity);

            var service = new CustomerService(repository, objectMapper);

            //Act
            var result = service.GetByCondition(1, null).Result;

            //Assert
            Assert.Equal(mockEntity.Id, result.Id);
        }

        [Fact]
        public void GetByConditionWithOnlyEmail()
        {
            //Arrange
            var mockEntity = new CustomerEntity { ContactEmail = "user@domain.com" };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());

            A.CallTo(() => repository.GetByCondition(null, mockEntity.ContactEmail)).Returns(mockEntity);

            var service = new CustomerService(repository, objectMapper);

            //Act
            var result = service.GetByCondition(null, "user@domain.com").Result;

            //Assert
            Assert.Equal(mockEntity.ContactEmail, result.ContactEmail);
        }

        [Fact]
        public void GetAllCustomers()
        {
            //Arrange
            var mockEntities = new CustomerEntity[]
            {
                new CustomerEntity { Id = 1, ContactEmail = "user1@domain.com", MobileNumber = "0123456", CustomerName = "Test 1" },
                new CustomerEntity { Id = 2, ContactEmail = "user2@domain.com", MobileNumber = "0123456", CustomerName = "Test 2" }
            };

            var repository = A.Fake<ICustomerRepository>();
            var objectMapper = CustomerInjuiryMapperBuilder.Build(new CustomerInjuiryProfile());

            A.CallTo(() => repository.GetAllAsync()).Returns(mockEntities);

            var service = new CustomerService(repository, objectMapper);

            //Act
            var result = service.GetCustomers().Result;

            //Assert
            Assert.Equal(2, result.Length);
        }

    }
}
